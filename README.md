Sutty .env remove
=================

Removes a `.env` file from a template.

Example Playbook
----------------

```yaml
---
- hosts: "sutty"
  strategy: "free"
  remote_user: "root"
  tasks:
  - block:
    - name: "role"
      include_role:
        name: "sutty_dotenv_create"
    always:
    - name: "role"
      include_role:
        name: "sutty_dotenv_remove"
```

License
-------

MIT-Antifa
